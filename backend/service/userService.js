class UserService {
    readAll(callback) {
        // return all users
        if (callback && typeof callback==="function"){
            callback()
        }
    }

    readone(id,callback) {
        // return one user by its id
        if (callback && typeof callback==="function"){
            callback()
        }
    }

    insert(data,callback) {
        // add a user
        if (callback && typeof callback==="function"){
            callback()
        }
    }

    update(data,id,callback) {
        // update a user by its id
        if (callback && typeof callback==="function"){
            callback()
        }
    }

    delete(id,callback) {
        // delete a user by its id
        if (callback && typeof callback==="function"){
            callback()
        }
    }
}
module.exports = new UserService();