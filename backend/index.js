const express = require('express');
const app = express();
const cors = require('cors');

const usersController = require('./controller/userController');

app.use(cors());
app.use(express.json());
app.use('/users',usersController);
app.use(express.static('www'));

app.get('/', function (req, res) {
  res.send('')
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
