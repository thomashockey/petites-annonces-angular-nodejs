var express = require('express');
var controller = express(); // Crée un serveur

//var users = require('./userController.js'); // appel du Json et des données

var users = [
    {nom:"John", email:"doe@gmail.com"},
    {nom:"Neo",email:"anderson@gmail.com"},
    {nom:"John",email:"wick@gmail.com"},
    {nom:"James",email:"cameron@gmail.com"}
];
//Simule une base de donnée



controller.get('/',function(req,res) {
    res.json(users)
});

controller.get('/:id',function(req,res) {
    res.send('id : '+req.params.id +
        '<br>nom : '+ users[req.params.id].name +
        '<br>email : '+users[req.params.id].email);
});

controller.post('/',function(req,res) {
    res.send('METHODE POST')
});

controller.put('/',function(req,res) {
    res.send('METHODE PUT')
});

controller.delete('/',function(req,res) {
    res.send('METHODE DELETE')
});

module.exports = controller;












