import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AccueilComponent } from './accueil/accueil.component';
import { AnnoncesComponent } from './annonces/annonces.component';
import { AuthoComponent } from './autho/autho.component';
import {RouterModule, Routes} from "@angular/router";
import { ProfilComponent } from './profil/profil.component';
import { ImageComponent } from './image/image.component';
import { CreateAnnoncesComponent } from './create-annonces/create-annonces.component';
import { NavbarComponent } from './navbar/navbar.component';

const r:Routes=[
  {path:"",component:AccueilComponent},
  {path:"annonce",component:AnnoncesComponent},
  {path:"autho", component:AuthoComponent},
  {path:"profil", component:ProfilComponent}
  ]
@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    AnnoncesComponent,
    AuthoComponent,
    ProfilComponent,
    ImageComponent,
    CreateAnnoncesComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(r)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
